const Course = require("../models/Course");

// Create a new course
// module.exports.addCourse = (reqBody) => {
// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	});
// 	return newCourse.save().then((course, error) => {
// 		// Course creation failed
// 		if (error) {
// 			return false;
// 		// Course creation successful
// 		} else {
// 			return true;
// 		};
// 	});
// };


module.exports.addCourse = (data) => {
	// User is an admin
	if (data.isAdmin) {
		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});
		// Saves the created object to our database
		return newCourse.save().then((course, error) => {
			// Course creation successful
			if (error) {
				return false;
			// Course creation failed
			} else {
				return true;
			};
		});
	
		} 

		let message = Promise.resolve('User must be ADMIN to access this!')
		return message.then((value) => {
			return {value}
		})
	// User is not an admin

};

// Retrieve ALL course

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}


// Retrieve ACTIVE courses

module.exports.getActiveCourses = (data) => {
	return User.findById(data.userId).then(result => {
		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		result.password = "";
		// Returns the user information with the password as an empty string
		return result;
	});
};


module.exports.getAllActive = () => {
  return Course.find({ isActive: true }).then (result => {
  	return result;
  });
 }

/////////////
 module.exports.inactive = () => {
  return Course.find({ isActive: false }).then (result => {
  	return result;
  });
 }
/////////////
 

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

//update a course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		} else {
			return true
		};
	});
};


//[SECTION] Activity 39

/*module.exports.addCourse = (reqBody, user) => {
  // Check if user is admin
  if (!user.isAdmin) {
    return Promise.reject("Unauthorized");
  }
  
  let newCourse = new Course({
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price
  });
  
  return newCourse.save()
    .then((course) => {
      return true;
    })
    .catch((error) => {
      return false;
    });
};

*/

// [SECTION] Activity 40
/*
module.exports.archiveCourse = (reqParams, reqBody) => {

  const courseActive = {
    		isActive: reqBody.isActive
 		 				}
  return Course.findByIdAndUpdate(reqParams.courseId, courseActive).then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		};
	});

  		let message = Promise.resolve('User must be ADMIN to access this!')
		return message.then((value) => {
			return {value}
		})
};*/

//////
module.exports.archiveCourse = (reqParams) => {

  const updateActiveField = {
    		isActive: true
 		 				}
  return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		};
	});

  	/*	let message = Promise.resolve('User must be ADMIN to access this!')
		return message.then((value) => {
			return {value}
		})*/
};